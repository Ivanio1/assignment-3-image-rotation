//
// Created by Asus on 15.12.2022.
//
#include "../include/image_object.h"

//Выделение памяти под новую картинку
void create_image(size_t width, size_t height, struct image *new_img) {
    struct pixel *pixels = malloc(width * height * sizeof(struct pixel));
    new_img->width = width;
    new_img->height = height;
    new_img->pixels = pixels;

}

//Освобождение памяти картинки
void free_image(const struct image *img) {
    free(img->pixels);
}
