//
// Created by Asus on 15.12.2022.
//
#include "header.h"
#include "padding.h"

//Получение заголовка
enum read_status get_header(FILE *file, struct bmp_header *header) {
    if (fread(header, sizeof(struct bmp_header), 1, file)) {
        return READ_OK;
    } else {
        return READ_INVALID_HEADER;
    }
}

//Создание заголовка
void create_header(const struct image *img, struct bmp_header * header) {
    uint32_t img_size = (sizeof(struct pixel) * img->width + padding_calc(img->width)) * img->height;
    *header = (struct bmp_header) {
            .bfType = TYPE,
            .bfileSize = sizeof(struct bmp_header) + img_size,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = INFO_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BITCount,
            .biCompression = COMPRESSION,
            .biSizeImage = img_size,
            .biXPelsPerMeter = XPels,
            .biYPelsPerMeter = YPels,
            .biClrUsed = ClrUsed,
            .biClrImportant = ClrImportant
    };
}

//Запись заголовка
enum write_status write_header(FILE *file, struct bmp_header *header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, file) == 1) {
        return WRITE_OK;
    } else {
        return WRITE_ERROR;
    }
}


