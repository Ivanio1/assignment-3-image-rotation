//
// Created by Asus on 15.12.2022.
//
#include "transform_object.h"
#include "bmp_reader.h"
#include "bmp_writer.h"


int main(int argc, char **argv) {
    if (argc == 3) { //1-exe,2-input,3-output
        FILE *in = fopen(argv[1], "rb");
        if (in == NULL) {
            fprintf(stderr, "%s\n","Неверный аргумент");
            return 1;
        } else {
            struct image data = {0};
            if (read_bmp(in, &data) == READ_OK) {
                struct image result_image = rotate(&data);
                free_image(&data);
                FILE *out = fopen(argv[2], "wb");
                if (out != NULL) {
                    if (write_bmp(out, &result_image) == WRITE_ERROR) {
                       fprintf(stderr, "%s\n","Ошибка конвертации");
                        return 1;
                    } else {
                        free_image(&result_image);
                    }

                } else {
                   fprintf(stderr, "%s\n","Неверный аргумент");
                    return 1;
                }
                fclose(out);
                fclose(in);
            } else {
               fprintf(stderr, "%s\n","Ошибка конвертации");
                return 1;
            }
        }
    } else {
        fprintf(stderr, "%s\n","Неверное количество аргументов");
        return 1;
    }
    return 0;
}
