//
// Created by Asus on 15.12.2022.
//
#include "bmp_reader.h"
#include "header.h"
#include "padding.h"


enum read_status read_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    if (get_header(in, &header) == READ_OK) {
        img->width = header.biWidth;
        img->height = header.biHeight;
        size_t new_width = img->height;
        size_t new_height = img->width;
        struct image new;
        create_image(new_height, new_width,&new);
        if (new.pixels == NULL) return READ_INVALID_BITS;
        size_t size_of_pixel=sizeof(struct pixel);
        for (size_t i = 0; i < new.height; ++i) {
            if (fread(new.pixels + (i * new.width), size_of_pixel, new.width, in) != new.width) {  //считываем построчно
                free(new.pixels);
                return READ_INVALID_SIGNATURE;
            }
            if (fseek(in, (long)padding_calc(new.width), SEEK_CUR) != 0) {                        //добавили padding
                free(new.pixels);
                return READ_INVALID_SIGNATURE;
            }
        }
        img->pixels = new.pixels;
        return READ_OK;
    } else {
        return READ_INVALID_SIGNATURE;
    }
}



