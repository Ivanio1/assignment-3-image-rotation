//
// Created by Asus on 15.12.2022.
//
#include "padding.h"
#include "image_object.h"


size_t padding_calc(uint64_t width) {
    size_t result = (width * sizeof(struct pixel)) % PADDING;
    if (result != 0) {
        return PADDING - result;
    } else {
        return 0;
    }

}
