//
// Created by Asus on 15.12.2022.
//
#include "bmp_writer.h"
#include "header.h"
#include "padding.h"

enum write_status write_bmp(FILE *out, const struct image *img) {
    struct bmp_header header;
    create_header(img, &header);
    uint8_t tmp_bytes[3] = {0, 0, 0};
    size_t padding = padding_calc(img->width);
    size_t count = 0;

    if(!fwrite(&header, sizeof (struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }
    if(fseek(out, header.bOffBits, SEEK_SET)){
        return WRITE_ERROR;
    }
   
    for (size_t i = 0; i < img->height; ++i) {
        struct pixel *to_write_pixel = ((*img).pixels + count);
        size_t written = fwrite(to_write_pixel, sizeof (struct pixel), img->width, out);
        if(written < img->width){
            return WRITE_ERROR;
        }
        count += img->width;
        if(padding != 0){
            size_t written_padding = fwrite(&tmp_bytes, sizeof (uint8_t), padding, out);
            if(written_padding < padding){
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;

}
