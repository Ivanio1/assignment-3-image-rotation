//
// Created by Asus on 15.12.2022.
//
#include "../include/transform_object.h"
#include "image_object.h"


struct image rotate(const struct image *src ) {
    struct image copy_image ;
    create_image(src->height,src->width,&copy_image);
    if (copy_image.pixels == NULL) {
        return (struct image){0};
    }
    for (size_t i = 0; i < src->height; i++) {
        for (size_t j = 0; j < src->width; j++) {
            size_t new_index=src->height*j + src->height-i-1;
            size_t old_index=src->width*i + j;
            copy_image.pixels[new_index] = src->pixels[old_index];
        }
    }
    return copy_image;
}
