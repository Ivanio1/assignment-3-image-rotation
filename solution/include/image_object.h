//
// Created by Asus on 15.12.2022.
//
#ifndef IMAGE_OBJECT_H // если имя IMAGE_OBJECT_H ещё не определено
#define IMAGE_OBJECT_H // определить имя IMAGE_OBJECT_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
}__attribute__((packed));

struct image {
    uint64_t width, height;
    struct pixel *pixels;
};

void create_image(size_t width, size_t height, struct image *new_img);

void free_image(const struct image *img);

#endif //IMAGE_OBJECT_H
// если  имя IMAGE_OBJECT_H уже определено, повторно не определять
