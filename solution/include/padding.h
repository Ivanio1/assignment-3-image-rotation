//
// Created by Asus on 15.12.2022.
//
#ifndef IMAGE_TRANSFORMER_PADDING_H
#define IMAGE_TRANSFORMER_PADDING_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


#define PADDING 4

size_t padding_calc(uint64_t width);

#endif //IMAGE_TRANSFORMER_PADDING_H
