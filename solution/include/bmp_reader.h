//
// Created by Asus on 15.12.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_READER_H
#define IMAGE_TRANSFORMER_BMP_READER_H
#include "./image_object.h"
#include "bmp_utils.h"


// Функция конвертации из формата BMP
enum read_status read_bmp(FILE *in, struct image *img);

#endif //IMAGE_TRANSFORMER_BMP_READER_H
