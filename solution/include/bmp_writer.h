//
// Created by Asus on 15.12.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_WRITER_H
#define IMAGE_TRANSFORMER_BMP_WRITER_H

#include "bmp_utils.h"

// Функция конвертации в формат BMP
enum write_status write_bmp(FILE *out, struct image const *img);
#endif //IMAGE_TRANSFORMER_BMP_WRITER_H
