//
// Created by Asus on 15.12.2022.
//
#ifndef TRANSFROM_OBJECT_H // если имя TRANSFROM_OBJECT_H ещё не определено
#define TRANSFROM_OBJECT_H // определить имя TRANSFROM_OBJECT_H

#include <stdint.h>
#include <stdio.h>

struct image rotate(const struct image *src);
#endif //TRANSFROM_OBJECT_H
// если  имя TRANSFROM_OBJECT_H уже определено, повторно не определять
