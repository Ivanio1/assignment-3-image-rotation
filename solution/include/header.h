//
// Created by Asus on 15.12.2022.
//
#ifndef IMAGE_TRANSFORMER_HEADER_H
#define IMAGE_TRANSFORMER_HEADER_H

#include "../include/bmp_utils.h"

#define TYPE 19778
#define PLANES 1
#define INFO_SIZE 40
#define BITCount 24
#define COMPRESSION 0
#define XPels 0
#define YPels 0
#define ClrUsed 0
#define ClrImportant 0


enum read_status get_header(FILE *file, struct bmp_header *header);
void create_header(const struct image *img, struct bmp_header * header);
enum write_status write_header(FILE *file, struct bmp_header *header);


#endif //IMAGE_TRANSFORMER_HEADER_H
